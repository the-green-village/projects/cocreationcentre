import importlib
import requests
import pandas as pd
import datetime as dt
import logging

import apikey
from apikey import ACCESS_TOKEN
from projectsecrets import TENANT_ID
from tgvfunctions import tgvfunctions
import json

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)

# Define start and endtime of request.
now = tgvfunctions.roundTime(utc=True)
firstTime = dt.datetime(2024,8,9,11,00)
lastTime = dt.datetime(2024,8,9,12,00)
endTime = now - dt.timedelta(minutes=5)
startTime = endTime - dt.timedelta(minutes=5)

# Define headers and parameters
Headers = {"Authorization": "Bearer " + ACCESS_TOKEN}
#Params = {"StartDate": "2021-9-11"}  # needed for downloading variable summary only

# Obtain the assetid
url_assets = "https://api.priva.com/data-insight-metadata/api/v1/tenants/" + TENANT_ID + "/assets"
response = requests.get(url_assets, headers=Headers)
# Select "CONVERGE", and not "Installaties prototype MOR TUDelft"
for setup in response.json():
    if setup['name'] == 'CONVERGE':
        asset_id = setup['id']


# Fetch the metadata of the variables
url_metadata = "https://api.priva.com/data-insight-metadata/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables"
response = requests.get(url_metadata, headers=Headers)
metadata = pd.json_normalize(response.json())
specificvariables = metadata.loc[metadata["displayName"].str.contains("Occupancy") | metadata["displayName"].str.contains("occupancy") | metadata["displayName"].str.contains("Room Light")]


# Load the variable summary from file
with open('historical_variableIds') as f:
   historical_variables = f.read().split("\n")

historical_variables = [e for e in historical_variables if e != '']
variablesummary = metadata[metadata["variableId"].isin(historical_variables)]
variablesummary = variablesummary[["deviceGroupId", "deviceId", "variableId"]].reset_index(drop=True)

'''
    variablelist = []
    for historical_variable_id in f.readlines():
        variablelist.append(historical_variable_id)
        historical_variables.append(metadata.loc[metadata["variableId"].str.equals(historical_variable_id)])
'''
# Fetch the variable summary
'''
url_data_summary = "https://api.priva.com/data-insight-history/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables/summary"
response = requests.get(url_data_summary, headers=Headers, params=Params)
variablesummary = pd.json_normalize(response.json())
'''

# Create the producer instance
producer = tgvfunctions.makeproducer('co-creation_centre-producer')

# Specify the request variables
url_data_variables = "https://api.priva.com/data-insight-history/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables"


def make_request(url_data_variables, Headers, subdf, startTime, endTime):
    specifics = subdf[["deviceGroupId", "deviceId", "variableId"]]
    variables = []
    for idx, var in specifics.iterrows():
        variables.append({"deviceGroupId": var["deviceGroupId"], "deviceId": var["deviceId"], "variableId": var["variableId"]})

    Body = {"variables": variables, "startTime": startTime.replace(microsecond=0, second=0).isoformat(), "endTime": endTime.replace(microsecond=0, second=0).isoformat()}
    return requests.post(url_data_variables, headers=Headers, json=Body)

# initialize start and end time
endTime = lastTime
startTime = endTime - dt.timedelta(minutes=5)

# Start at lastTime and move backwards in time
while startTime >= firstTime:
    # reload Headers
    importlib.reload(apikey)
    from apikey import ACCESS_TOKEN
    Headers = {"Authorization": "Bearer " + ACCESS_TOKEN}
    logger.info(f"new start time: {startTime}")
    
    # Fetch the historic data for all the variables in the specified timeframe
    r = 0
    rows_count = len(variablesummary)
    variables_per_request = 50
    looping = True
    while looping:
        end = r + variables_per_request
        if end < rows_count:
            subdf = variablesummary.iloc[r: end]
            r = end
        else:
            subdf = variablesummary.iloc[r:]
        response = make_request(url_data_variables, Headers, subdf, startTime, endTime)
        for element in response.json():
            variableReference = element["variableReference"]
            variableId = variableReference["variableId"]
            deviceGroupId = variableReference["deviceGroupId"]
            deviceId = variableReference["deviceId"]
            metadatarow = metadata[metadata["variableId"] == variableId].iloc[0]
            measurement_id = metadatarow["displayName"]
            data = element["historicalValues"]
            if measurement_id[:9] == 'ClimAD - ':
                continue
            if data:
                for measurement in data:
                    Measurement = pd.json_normalize(measurement)
                    measurement["epochms"] = tgvfunctions.isotoepochms(Measurement,"timestampUtc")
                    measurement = json.loads(Measurement.to_json(orient="records"))[0]
                    value = {
                                "project_id": "co_creation_centre",
                                "application_id": asset_id,
                                "device_id": deviceId,
                                "timestamp": measurement["epochms"],
                                "measurements": [
                                    {
                                        "measurement_id": variableId,
                                        "measurement_description": measurement_id,
                                        "value": measurement["value"],
                                        "unit": metadatarow["unit"]
                                    }
                                ]
                            }
                    tgvfunctions.produce_fast(producer, value)
        if end >= rows_count:
            looping = False

    # update start and end time
    endTime = startTime
    startTime = endTime - dt.timedelta(minutes=5) 

    producer.flush()
producer.flush()
