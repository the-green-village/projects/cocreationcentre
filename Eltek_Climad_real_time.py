import requests
import pandas as pd
import datetime as dt
import os
from confluent_kafka import Producer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.serialization import SerializationContext, MessageField

from apikey import ACCESS_TOKEN
from projectsecrets import TOPIC_CLIMAD, TENANT_ID
import json

# Import Kafka secrets from environment
API_KEY = os.environ['API_KEY']
API_PASSWORD = os.environ['API_PASSWORD']
BOOTSTRAP_SERVERS = os.environ['BOOTSTRAP_SERVERS']
SCHEMA_REGISTRY_URL = os.environ['SCHEMA_REGISTRY_URL']
SCHEMA_REGISTRY_USERNAME = os.environ['SCHEMA_REGISTRY_USERNAME']
SCHEMA_REGISTRY_PASSWORD = os.environ['SCHEMA_REGISTRY_PASSWORD']

# Copied from tgvfunctions
def make_producer(producername):
    # Create the producer instance. For producername, typically use {project name}-producer
    producer = Producer({
        'client.id': producername,
        'bootstrap.servers': BOOTSTRAP_SERVERS,
        'sasl.mechanisms': 'PLAIN',
        'security.protocol': 'SASL_SSL',
        'sasl.username': API_KEY,
        'sasl.password': API_PASSWORD,
    })
    return producer


def round_time(Dt=None, roundTo=300, utc=False):
    # Round a datetime object to any datetime in seconds
    # dt : datetime.datetime object, defaults to now.
    # roundTo : Closest number of seconds to round to, default 5 minutes.
    # The default is no utc time.
    # Example: if it is now 11:27, t = roundTime() will result in t = 11:25 (in a datetime format)
    if Dt is None:
        if not utc:
            Dt = dt.datetime.now()
        else:
            Dt = dt.datetime.utcnow()
    seconds = (Dt.replace(tzinfo=None) - Dt.min).seconds
    rounding = (seconds) // roundTo * roundTo

    return Dt + dt.timedelta(0, rounding-seconds, -Dt.microsecond)


def isotoepochms(dataframe, datecolumnname):
    # Takes a dataframe an the columnname of the date column in iso, it then returns a dataframe with an extra column with the timestampepoch in ms named "epochms"
    dataframe["epochms"] = pd.to_datetime(dataframe[datecolumnname], infer_datetime_format=True)
    dataframe["epochms"] = dataframe["epochms"].astype("int64")//1e6
    return dataframe


# Define headers, schema conf etc. Do not change variables here but change in projectsecrets.py.
# The projectsecrets.py file should be in the code folder.
conf = {
    'url': SCHEMA_REGISTRY_URL,
    'basic.auth.user.info': f"{SCHEMA_REGISTRY_USERNAME}:{SCHEMA_REGISTRY_PASSWORD}"
}
schema_registry_client = SchemaRegistryClient(conf)
r = requests.get(f"{SCHEMA_REGISTRY_URL}/subjects/{TOPIC_CLIMAD}-value/versions/latest",
                 auth=(SCHEMA_REGISTRY_USERNAME, SCHEMA_REGISTRY_PASSWORD))
schema_str = r.json()['schema']
conf = {
    'auto.register.schemas': False
}
avro_serializer = AvroSerializer(schema_registry_client,
                                 schema_str,
                                 conf=conf)
ser_context = SerializationContext(TOPIC_CLIMAD, MessageField.VALUE)

# Define start and endtime of request.
now = round_time(utc=True)
endTime = now - dt.timedelta(minutes=60)
startTime = endTime - dt.timedelta(minutes=60)

# Define headers and parameters
Headers = {"Authorization": "Bearer " + ACCESS_TOKEN}
Params = {"StartDate": "2021-9-11"}

# Obtain the assetid
url_assets = "https://api.priva.com/data-insight-metadata/api/v1/tenants/" + TENANT_ID + "/assets"
response = requests.get(url_assets, headers=Headers)
# Select "CONVERGE", and not "Installaties prototype MOR TUDelft"
for setup in response.json():
    if setup['name'] == 'CONVERGE':
        asset_id = setup['id']

# Fetch the metadata of the variables
url_metadata = "https://api.priva.com/data-insight-metadata/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables"
response = requests.get(url_metadata, headers=Headers)
metadata = pd.json_normalize(response.json())
specificvariables = metadata.loc[metadata["displayName"].str.contains("ClimAD")]
specificvariables = specificvariables[["deviceGroupId", "deviceId", "variableId"]]

# Fetch the variable summary
url_data_summary = "https://api.priva.com/data-insight-history/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables/summary"
response = requests.get(url_data_summary, headers=Headers, params=Params)
variablesummary = pd.json_normalize(response.json())
variablesummary = pd.concat([variablesummary, specificvariables], ignore_index=True)

# Create the producer instance
producer = make_producer('co-creation_centre-Eltek_Climad-producer')

# Specify the request variables
url_data_variables = "https://api.priva.com/data-insight-history/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables"
Headers = {"Authorization": "Bearer " + ACCESS_TOKEN}


def make_request(url_data_variables, Headers, subdf):
    specifics = subdf[["deviceGroupId", "deviceId", "variableId"]]
    variables = []
    for idx, var in specifics.iterrows():
        variables.append(
            {"deviceGroupId": var["deviceGroupId"], "deviceId": var["deviceId"], "variableId": var["variableId"]})

    Body = {"variables": variables, "startTime": startTime.replace(microsecond=0, second=0).isoformat(),
            "endTime": endTime.replace(microsecond=0, second=0).isoformat()}
    return requests.post(url_data_variables, headers=Headers, json=Body)


# Fetch the historic data for all the variables in the specified timeframe
r = 0
rows_count = len(variablesummary)
variables_per_request = 50
looping = True
while looping:
    end = r + variables_per_request
    if end < rows_count:
        subdf = variablesummary.iloc[r: end]
        r = end
    else:
        subdf = variablesummary.iloc[r:]
    response = make_request(url_data_variables, Headers, subdf)
    for element in response.json():
        variableReference = element["variableReference"]
        variableId = variableReference["variableId"]
        deviceGroupId = variableReference["deviceGroupId"]
        deviceId = variableReference["deviceId"]
        metadatarow = metadata[metadata["variableId"] == variableId].iloc[0]
        measurement_id = metadatarow["displayName"]
        data = element["historicalValues"]
        if measurement_id[:9] == 'ClimAD - ':
            variableId = variableId.replace('ClimAD', 'ClimAd')
            if data:
                for measurement in data:
                    Measurement = pd.json_normalize(measurement)
                    measurement["epochms"] = isotoepochms(Measurement, "timestampUtc")
                    measurement = json.loads(Measurement.to_json(orient="records"))[0]
                    value = {
                        "project_id": "ClimAd",
                        "project_description": "ClimAd project in the Sustainer Homes that uses Eltek for data collection. \
                         The data are sent to an Eltek receiver connected to Priva in the Co-Creation Centre.",
                        "application_id": "Priva",
                        "application_description": asset_id,
                        "device_id": "Eltek",
                        "device_description": deviceId,
                        "device_type": "Eltek GenII RC250 Telemetry Receiver",
                        "timestamp": measurement["epochms"],
                        "measurements": [
                            {
                                "measurement_id": measurement_id,
                                "measurement_description": variableId,
                                "value": measurement["value"],
                                "unit": metadatarow["unit"]
                            }
                        ]
                    }
                    avro_value = avro_serializer(value, ser_context)
                    producer.produce(topic=TOPIC_CLIMAD, value=avro_value)
                    producer.poll(0)
                    producer.flush()
    if end >= rows_count:
        looping = False
producer.flush()
