#!/bin/bash
source ~/producers/.kafka_envs

cd ~/producers/co_creation_centre/code
. ../env/bin/activate
python real_time.py
