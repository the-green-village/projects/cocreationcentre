from projectsecrets import CLIENT_ID, CLIENT_SECRET
import requests
import time

# Define parameters
Headers = {'Content-Type': 'application/x-www-form-urlencoded'}
body = {'grant_type': 'client_credentials', 'client_id': CLIENT_ID, 'client_secret': CLIENT_SECRET, 'scope': 'priva.data-services'}


def apirequest():
    url_api_key_request = "https://auth.priva.com/connect/token"
    response = requests.post(url_api_key_request, headers=Headers, data=body)
    try:
        access_token = response.json()["access_token"]
    except Exception:
        time.sleep(1)
        access_token = apirequest()
    return access_token


access_token = apirequest()
file = open("apikey.py", 'w')
file.write("ACCESS_TOKEN = "+"\"" + access_token + "\"")
file.close()
