import requests
import pandas as pd
import datetime as dt

from apikey import ACCESS_TOKEN
from projectsecrets import TENANT_ID
from tgvfunctions import tgvfunctions
import json

# Define start and endtime of request.
now = tgvfunctions.roundTime(utc=True)
endTime = now - dt.timedelta(minutes=5)
startTime = endTime - dt.timedelta(minutes=5)

# Define headers and parameters
Headers = {"Authorization": "Bearer " + ACCESS_TOKEN}
#Params = {"StartDate": "2021-9-11"}  # needed for the variable summary only

# Obtain the assetid
url_assets = "https://api.priva.com/data-insight-metadata/api/v1/tenants/" + TENANT_ID + "/assets"
response = requests.get(url_assets, headers=Headers)
# Select "CONVERGE", and not "Installaties prototype MOR TUDelft"
for setup in response.json():
    if setup['name'] == 'CONVERGE':
        asset_id = setup['id']

# Fetch the metadata of the variables
url_metadata = "https://api.priva.com/data-insight-metadata/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables"
response = requests.get(url_metadata, headers=Headers)
metadata = pd.json_normalize(response.json())

# Fetch the variable summary
'''
url_data_summary = "https://api.priva.com/data-insight-history/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables/summary"
response = requests.get(url_data_summary, headers=Headers, params=Params)
variablesummary = pd.json_normalize(response.json())
'''
# Load the variable summary from file
with open('longterm.json') as f:
    variablesummary = pd.json_normalize(json.load(f))

# Create the producer instance
producer = tgvfunctions.makeproducer('co-creation_centre-producer')

# Specify the request variables
url_data_variables = "https://api.priva.com/data-insight-history/api/v1/tenants/" + TENANT_ID + "/assets/" + asset_id + "/variables"
#Headers = {"Authorization": "Bearer " + ACCESS_TOKEN}


def make_request(url_data_variables, Headers, subdf):
    specifics = subdf[["deviceGroupId", "deviceId", "variableId"]]
    variables = []
    for idx, var in specifics.iterrows():
        variables.append({"deviceGroupId": var["deviceGroupId"], "deviceId": var["deviceId"], "variableId": var["variableId"]})

    Body = {"variables": variables, "startTime": startTime.replace(microsecond=0, second=0).isoformat(), "endTime": endTime.replace(microsecond=0, second=0).isoformat()}
    return requests.post(url_data_variables, headers=Headers, json=Body)


# Fetch the historic data for all the variables in the specified timeframe
r = 0
rows_count = len(variablesummary)
variables_per_request = 50
looping = True
while looping:
    end = r + variables_per_request
    if end < rows_count:
        subdf = variablesummary.iloc[r: end]
        r = end
    else:
        subdf = variablesummary.iloc[r:]
    response = make_request(url_data_variables, Headers, subdf)
    for element in response.json():
        variableReference = element["variableReference"]
        variableId = variableReference["variableId"]
        deviceGroupId = variableReference["deviceGroupId"]
        deviceId = variableReference["deviceId"]
        metadatarow = metadata[metadata["variableId"] == variableId].iloc[0]
        measurement_id = metadatarow["displayName"]
        data = element["historicalValues"]
        if measurement_id[:9] == 'ClimAD - ':
            continue
        if data:
            for measurement in data:
                Measurement = pd.json_normalize(measurement)
                measurement["epochms"] = tgvfunctions.isotoepochms(Measurement,"timestampUtc")
                measurement = json.loads(Measurement.to_json(orient="records"))[0]
                value = {
                            "project_id": "co_creation_centre",
                            "application_id": asset_id,
                            "device_id": deviceId,
                            "timestamp": measurement["epochms"],
                            "measurements": [
                                {
                                    "measurement_id": variableId,
                                    "measurement_description": measurement_id,
                                    "value": measurement["value"],
                                    "unit": metadatarow["unit"]
                                }
                            ]
                        }
                tgvfunctions.produce(producer, value)
    if end >= rows_count:
        looping = False
producer.flush()
